﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.IO.Ports;
using System.Xml.Linq;
using System.Threading;


namespace newSMS_SERVER
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        static string port_name;
        static string pull;
        static string dfg;
        private static List<string> listfiles;


        class MyTread
        {
            Thread tread;

            private int id;

            public MyTread(int threadID)
            {              
                tread = new Thread(this.my_tread);
                tread.Start(threadID);
                this.id = threadID;
            }

            void my_tread(object putt)
            {
                int count2;

                System.IO.Ports.SerialPort port = new System.IO.Ports.SerialPort();
                //далее необходимо настроить порт для работы с мобильным телефоном
                port.PortName = port_name;
                //Время ожидания записи и чтения с порта
                port.WriteTimeout = 500; port.ReadTimeout = 500;

                //Другие необходимые настройки - подходит для большинства телефонов - но возможно придется настраивать:
                port.BaudRate = 115200;
                port.Parity = Parity.None;
                port.DataBits = 8;
                port.StopBits = StopBits.One;
                port.Handshake = Handshake.RequestToSend;
                port.DtrEnable = true;
                port.RtsEnable = true;
                port.NewLine = System.Environment.NewLine;

                string filename = Form1.listfiles[this.id];
                if (filename != null)
                {
                    port.Open();

                    StreamReader new_read = new StreamReader(filename);
                    count2 = Convert.ToInt32(new_read.ReadLine());
                    MessageBox.Show(port.PortName);
                    for (int j = 1; j <= count2; j++)
                    {
                        //Далее можем работать с телефоном посредством AT команд
                        //К примеру, набор номера
                        pull = new_read.ReadLine();
                        port.WriteLine(pull);
                        System.Threading.Thread.Sleep(2000);
                        pull = new_read.ReadLine();
                        port.WriteLine(pull);
                        System.Threading.Thread.Sleep(6000);
                    }


                    dfg = port.ReadExisting();
                    //И конечно же, не забываем закрывать порты
                    port.Close();
                    MessageBox.Show(dfg);
                    new_read.Close();
                    File.Delete(filename);
                    Form1.listfiles[this.id] = null;
                }

            }
        }

            private void button1_Click(object sender, EventArgs e)
            {
                while (true)
                {
                    start_e();
                }
            }

            void start_e()
            {
                string[] n_p = File.ReadAllLines(@"C:\SMS\COM.txt");
                int numPorts = n_p.Length;
                string[] dirfiles = Directory.GetFiles(@"C:\SMS\sends\", "*.txt");
                if (dirfiles != null)
                {
                    Form1.listfiles = new List<string>();
                    {
                        for (int i = 0; i < numPorts; i++)
                        {
                            listfiles.Add(dirfiles[i]);
                        }

                        for (int k = 0; k <= numPorts - 1; k++)
                        {
                            MessageBox.Show(n_p[k]);
                            port_name = n_p[k];

                            MyTread tread = new MyTread(k);
                            System.Threading.Thread.Sleep(5000);
                        }
                    }
                }
                else start_e();

                }
            
        
            private void Form1_Load(object sender, EventArgs e)
            {
                XDocument xDocument = XDocument.Load(@"C:\SMS\settings.xml");
            }


        
    }
}
